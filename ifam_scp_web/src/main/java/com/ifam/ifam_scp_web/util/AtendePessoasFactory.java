package com.ifam.ifam_scp_web.util;

import core.soap.services.AtendePessoas;
import core.soap.services.AtendePessoasEJB;
import core.soap.services.Cidade;
import core.soap.services.Estado;
import core.soap.services.Logradouro;
import core.soap.services.Pessoa;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author erick.araujo
 */

@ApplicationScoped
public class AtendePessoasFactory {
    
    Logger log = Logger.getLogger(AtendePessoasFactory.class);

    AtendePessoas service;
    
    @PostConstruct
    public void init(){
        log.info("init class atendePessoasFactory");
        if(this.service == null){
            this.service = new AtendePessoas();
        }
    }
    
    private AtendePessoasEJB startAPFactory(){
        log.info("init startApFactory");
        log.info("startApFactory service -> " + service);
        AtendePessoasEJB port = service.getAtendePessoasEJBPort();   
        return port;
    }
    
    public boolean cadastrarPessoa(Pessoa p){
        log.info("cadastrando pessoa");
        if(p == null){
            log.info("pessoa nula");
            return false;
        }
        log.info("pessoa -> " + startAPFactory().toString(p));
        startAPFactory().cadastrarPessoa(p);
        log.info("pessoa cadastrada");
        return true;
    }
    
    public void removerPessoa(Pessoa p){
        try{
            startAPFactory().deletaPessoa(p);
            log.info("Sucesso ao remover!");
        }catch(Exception e){
            log.info("falha ao remover pessoa");
        }
    }
    
    public List<Pessoa> listarPessoas(){
        log.info("listando pessoas");
        return startAPFactory().listarTodasPessoas();
    }
    
    public List<Pessoa> pesquisaPessoaNome(String pesquisaNome){
        log.info("pesquisa pessoa por nome");
        return startAPFactory().pesquisaPessoaPorNome(pesquisaNome);
    }
    
    public String pessoaToString(Pessoa p){
        log.info("pessoa toString");
        return startAPFactory().toString(p);
    }
    
    public boolean cadastrarLogradouro(Logradouro cep){
        log.info("cadastrando cep");
        if(cep == null){
            log.info("cep nula");
            return false;
        }
        log.info("cep -> " + cep.getCep());
        startAPFactory().cadastraCep(cep);
        log.info("cep cadastrada");
        return true;
    }
    
    public List<Logradouro> listarLogradouros(){
        log.info("listando ceps");
        return startAPFactory().listarTodosCeps();
    }
    
    public Logradouro pesquisaCep(String pesquisaCep){
        log.info("pesquisa cep");
        return startAPFactory().consultaCep(pesquisaCep);
    }
    
    public boolean cadastrarCidade(Cidade cid){
        log.info("cadastrando cidade");
        if(cid == null){
            log.info("cidade nula");
            return false;
        }
        log.info("cidade -> " + cid.getNome());
        startAPFactory().cadastrarCidade(cid);
        log.info("cidade cadastrada");
        return true;
    }
    
    public List<Cidade> listarCidades(){
        log.info("listando cidades");
        return startAPFactory().listarTodasCidades();
    }
    
    public List<Cidade> pesquisaCidadeNome(String pesquisaNome){
        log.info("pesquisa cidade por nome");
        return startAPFactory().listaCidadePorNome(pesquisaNome);
    }
    
    public Cidade pesquisaCidadePorNomedEstado(String cidade_nome, String estado_nome){
        log.info("pesquisa cidade por nome dela e de estado");
        return startAPFactory().listaCidadePorNomedEstado(cidade_nome, estado_nome);
    }
    
    public List<Cidade> pesquisaCidadePorEstado(String estado){
        log.info("pesquisa cidade por nome de estado");
        return startAPFactory().listaCidadePorEstado(estado);
    }
    
    public boolean cadastrarEstado(Estado estado){
        log.info("cadastrando estado");
        if(estado == null){
            log.info("estado nulo");
            return false;
        }
        log.info("estado -> " + estado.getNome());
        String res = startAPFactory().cadastrarEstado(estado);
        log.info("feedback cidade -> " + res);
        log.info("estado cadastrada");
        return true;
    }
    
    public List<Estado> listarEstados(){
        log.info("listando estados");
        return startAPFactory().listarTodosEstados();
    }
    
    public Estado pesquisaEstadoNome(String pesquisaNome){
        log.info("pesquisa estado por nome");
        return startAPFactory().listarEstadosPorNome(pesquisaNome);
    }
    
    public Estado pesquisaEstadoPorSigla(String sigla){
        log.info("pesquisa estado por sigla");
        return startAPFactory().listarEstadoPorSigla(sigla);
    }
}
