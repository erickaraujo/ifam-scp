package com.ifam.ifam_scp_web.util;

import br.com.correios.bsb.sigep.master.bean.cliente.AtendeCliente;
import br.com.correios.bsb.sigep.master.bean.cliente.AtendeClienteService;
import br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP;
import br.com.correios.bsb.sigep.master.bean.cliente.SQLException_Exception;
import br.com.correios.bsb.sigep.master.bean.cliente.SigepClienteException;
import core.soap.services.Cidade;
import core.soap.services.Estado;
import core.soap.services.Logradouro;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author erick.araujo
 */

@ApplicationScoped
public class CorreioServiceFactory {
    
    Logger log = Logger.getLogger(CorreioServiceFactory.class);
    
    AtendeClienteService service;
    
    @PostConstruct
    public void init(){
        log.info("init correios webservice factory");
        if(this.service == null){
            this.service = new AtendeClienteService();
        }
    }
    
    private AtendeCliente startAcFactory(){
        log.info("init startAcFactory");
        log.info("startAcFactory -> " + service);
        
        AtendeCliente port = service.getAtendeClientePort();
        return port;
    }
    
    public Logradouro consultaCep(String cep){
        try {
            log.info("correioserviceFactory");
            EnderecoERP endErp = startAcFactory().consultaCEP(cep);
            
            log.info("teste correioService -> " + endErp.getEnd()+ ", " 
                    + endErp.getCep() + ", " + endErp.getBairro() + ", "
                    + endErp.getCidade()+ ", " + endErp.getUf() + ", "
                    + endErp.getComplemento() + ", " + endErp.getComplemento2() +
                    " ---- " + endErp.getUnidadesPostagem());
            
            Estado estado = new Estado();
            estado.setSigla(endErp.getUf());
            
            Cidade cidade = new Cidade();
            cidade.setNome(endErp.getCidade());
            cidade.setEstado(estado);
            
            Logradouro logradouro = new Logradouro();
            logradouro.setLogradouro(endErp.getEnd());
            logradouro.setCep(endErp.getCep());
            logradouro.setBairro(endErp.getBairro());
            logradouro.setLocalidade(cidade);
            
            log.info("teste endErp -> " + endErp.toString());
            log.info("teste logradouro -> " + logradouro.getLogradouro() + ", " 
                    + logradouro.getCep() + ", " + logradouro.getBairro() + ", "
                    + cidade.getNome()  + ", " + estado.getNome());
            return logradouro;
        } catch (SQLException_Exception ex) {
            java.util.logging.Logger.getLogger(CorreioServiceFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SigepClienteException ex) {
            java.util.logging.Logger.getLogger(CorreioServiceFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
}
