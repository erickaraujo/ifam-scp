package com.ifam.ifam_scp_web.controller;

import com.ifam.ifam_scp_web.util.AtendePessoasFactory;
import com.ifam.ifam_scp_web.util.CorreioServiceFactory;
import core.soap.services.Cidade;
import core.soap.services.Estado;
import core.soap.services.Logradouro;
import core.soap.services.Pessoa;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.apache.log4j.Logger;

import static com.ifam.ifam_scp_web.util.FaceUtils.redirect;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author erick.araujo
 */
@ViewScoped
@ManagedBean
public class PessoaController implements Serializable {

    Logger log = Logger.getLogger(PessoaController.class);
    private String pesquisa_cep;
    private Pessoa pessoa;
    private Logradouro logradouro;
    private Cidade cidade;
    private Estado estado;

    private List<Estado> listaEstados;
    private List<Cidade> listaCidades;

    @Inject
    CorreioServiceFactory correiosFactory;
    @Inject
    AtendePessoasFactory apFactory;

    @PostConstruct
    public void init() {
        log.info("init pessoa controller");
        if (pessoa == null) {
            pessoa = new Pessoa();
        }
        if (estado == null) {
            estado = new Estado();
        }
        if (cidade == null) {
            cidade = new Cidade();
        }

        if (listaEstados == null) {
            populaEstados();
        }
    }

    public Logradouro pesquisaCep() {
        logradouro = new Logradouro();
        log.info("pesquisaCep metodo");
        logradouro = (apFactory.pesquisaCep(pesquisa_cep) != null) ? apFactory.pesquisaCep(pesquisa_cep) : correiosFactory.consultaCep(pesquisa_cep);
        log.info("logradouro string -> " + logradouro.toString());
        log.info("logradouro cep -> " + logradouro.getCep());
        log.info("logradouro bairro -> " + logradouro.getBairro());
        log.info("logradouro cidade -> " + logradouro.getLocalidade().getNome());
        log.info("logradouro estado sigla -> " + logradouro.getLocalidade().getEstado().getSigla());
        estado = apFactory.pesquisaEstadoPorSigla(logradouro.getLocalidade().getEstado().getSigla());
        log.info("estado selecionado -> " + estado.getNome());
        cidade = apFactory.pesquisaCidadePorNomedEstado(logradouro.getLocalidade().getNome(), estado.getNome());
        log.info("cidade selecionado -> " + cidade.getNome());
        return logradouro;
    }

    public String submitCadastro() {
        log.info("submit cadastro!");
        try {
            estado = apFactory.pesquisaEstadoNome(estado.getNome());
            cidade = apFactory.pesquisaCidadePorNomedEstado(cidade.getNome(), estado.getNome());
            cidade.setEstado(estado);
            logradouro.setLocalidade(cidade);
            pessoa.setLogradouro(logradouro);
            pessoa.setNascimento(dateToXMLCalendar("10-02-1996"));

            log.info("estado id " + estado.getId());
            log.info("cidade id " + cidade.getId());
            apFactory.cadastrarEstado(estado);
            apFactory.cadastrarCidade(cidade);
            apFactory.cadastrarLogradouro(logradouro);
            apFactory.cadastrarPessoa(pessoa);
            log.info("sucesso!");
            return redirect("homePagePessoa");
        } catch (Exception e) {
            log.info("Exception!");
            log.info(e);
            return null;
        }
    }

    public void populaEstados() {
        log.info("metodo populaEstados");
        listaEstados = apFactory.listarEstados();

    }

    public void populaCidade() {
        log.info("metodo populaCidades");
        listaCidades = new ArrayList();
        
        if (estado != null && estado.getNome() != null) {
            log.info("entrou no if \n estado nome -> " + estado.getNome());
            listaCidades = apFactory.pesquisaCidadePorEstado(estado.getNome());;
        }
    }
    
    private static XMLGregorianCalendar dateToXMLCalendar(String data) throws ParseException, DatatypeConfigurationException{
        if(data==null){
            return null;
        }
        GregorianCalendar gregCalendar = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
        Date date = sdf.parse(data);
        gregCalendar.setTime(date);
        XMLGregorianCalendar xmlDate;
        xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregCalendar);
        
        return xmlDate;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getPesquisa_cep() {
        return pesquisa_cep;
    }

    public void setPesquisa_cep(String pesquisa_cep) {
        this.pesquisa_cep = pesquisa_cep;
    }

    public Logradouro getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(Logradouro logradouro) {
        this.logradouro = logradouro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Estado> getListaEstados() {
        return listaEstados;
    }

    public void setListaEstados(List<Estado> listaEstados) {
        this.listaEstados = listaEstados;
    }

    public List<Cidade> getListaCidades() {
        return listaCidades;
    }

    public void setListaCidades(List<Cidade> listaCidades) {
        this.listaCidades = listaCidades;
    }

}
