package com.ifam.ifam_scp_web.controller;

import com.ifam.ifam_scp_web.util.AtendePessoasFactory;
import com.ifam.ifam_scp_web.util.CorreioServiceFactory;
import core.soap.services.Logradouro;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author erick.araujo
 */
@ViewScoped
@ManagedBean
public class LogradouroController implements Serializable{
    Logger log = Logger.getLogger(LogradouroController.class);
    
    private Logradouro logradouro;
    private List<Logradouro> logradouros;
    private String pesquisa_cep;
    
    @Inject
    CorreioServiceFactory correiosFactory;
    @Inject
    AtendePessoasFactory apFactory; 
            
    @PostConstruct
    public void init(){
        log.info("init logradouro controller");
        if(logradouro == null){
            log.info("logradouro null, initializing...");
            logradouro = new Logradouro();
        }
        log.info("logradouro = " + logradouro);
    }
    
    public Logradouro pesquisaCep(){
        log.info("pesquisaCep metodo");
        logradouro = (apFactory.pesquisaCep(pesquisa_cep) != null) ? apFactory.pesquisaCep(pesquisa_cep) : correiosFactory.consultaCep(pesquisa_cep);
        log.info("logradouro -> " + logradouro.toString());
        log.info("logradouro -> " + logradouro.getCep());
        log.info("logradouro -> " + logradouro.getBairro());
        return logradouro;
    }

    public Logradouro getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(Logradouro logradouro) {
        this.logradouro = logradouro;
    }

    public String getPesquisa_cep() {
        return pesquisa_cep;
    }

    public void setPesquisa_cep(String pesquisa_cep) {
        this.pesquisa_cep = pesquisa_cep;
    }

    public List<Logradouro> getLogradouros() {
        return logradouros;
    }

    public void setLogradouros(List<Logradouro> logradouros) {
        this.logradouros = logradouros;
    }
}
