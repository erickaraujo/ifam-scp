package com.ifam.ifam_scp_web.controller;

import com.ifam.ifam_scp_web.util.AtendePessoasFactory;
import core.soap.services.Pessoa;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author erick.araujo
 */

@ViewScoped
@ManagedBean
public class ListarPessoaController implements Serializable{
    Logger log = Logger.getLogger(ListarPessoaController.class);
    
    private List<Pessoa> pessoas;
    private String pesquisaNome;
    
    @Inject
    AtendePessoasFactory apFactory;
    
    @PostConstruct
    public void init(){
        log.info("init listarPessoas");
        pesquisaPessoa();
    }
    
    public void pesquisaPessoa(){
        if(pesquisaNome == null || pesquisaNome.isEmpty()){
            log.info("pesquisaNome vazio!");
            pessoas = apFactory.listarPessoas();
            log.info("listando todas as pessoas!");
        }
        else{
            pessoas = apFactory.pesquisaPessoaNome(pesquisaNome);
            log.info("pesquisando pessoas com '" + pesquisaNome+ "'");
        }
    }
    
    public void removePessoa(Pessoa p){
            apFactory.removerPessoa(p);
    }
    
    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }
    
    public String getPesquisa_nome() {
        return pesquisaNome;
    }

    public void setPesquisa_nome(String pesquisa_nome) {
        this.pesquisaNome = pesquisa_nome;
    }
}
