package com.ifam.ifam_scp_web.converter;

import core.soap.services.Logradouro;
import core.soap.services.Pessoa;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import javax.faces.convert.FacesConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author erick.araujo
 */
//@RequestScoped
public class PessoaConverter{
   private Pessoa pessoa;
   private Date nascimento;

//   @PostConstruct
   public void init(){
        if(pessoa == null){
            pessoa = new Pessoa();   
        }
   }

    public String getNome() {
        return pessoa.getNome();
    }

    public void setNome(String nome) {
        pessoa.setNome(nome);
    }

    public Logradouro getLogradouro() {
        return pessoa.getLogradouro();
    }

    public void setLogradouro(Logradouro logradouro) {
        pessoa.setLogradouro(logradouro);
    }

    public String getSexo() {
        return pessoa.getSexo();
    }

    public void setSexo(String sexo) {
        pessoa.setSexo(sexo);
    }

    public Date getNascimento() {
        
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
       try {
           pessoa.setNascimento(dateToXMLCalendar(nascimento));
       } catch (ParseException ex) {
           Logger.getLogger(PessoaConverter.class.getName()).log(Level.SEVERE, null, ex);
       } catch (DatatypeConfigurationException ex) {
           Logger.getLogger(PessoaConverter.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    private static XMLGregorianCalendar dateToXMLCalendar(Date data) throws ParseException, DatatypeConfigurationException{
        if(data==null){
            return null;
        }
        GregorianCalendar gregCalendar = new GregorianCalendar();
        gregCalendar.setTime(data);
        XMLGregorianCalendar xmlDate;
        xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregCalendar);
        
        return xmlDate;
    }
    
    private static Date XMLCalendarToDate(XMLGregorianCalendar data){
        String pattern = "yyyy-MM-dd'T'HH:mm:ss";
        DateFormat formatter = new SimpleDateFormat(pattern);
        Date date = data.toGregorianCalendar().getTime();
        return date;
    }
}
