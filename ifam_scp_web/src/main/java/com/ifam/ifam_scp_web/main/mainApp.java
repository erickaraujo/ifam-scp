package com.ifam.ifam_scp_web.main;

import core.soap.services.AtendePessoas;
import core.soap.services.AtendePessoasEJB;
import core.soap.services.Pessoa;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author erick.araujo
 */
public class mainApp {
   
    public static void main(String[] args) {
        setPessoa();
        listarPessoas();
    }
    
    private static XMLGregorianCalendar dateToXMLCalendar(String data) throws ParseException, DatatypeConfigurationException{
        if(data==null){
            return null;
        }
        GregorianCalendar gregCalendar = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
        Date date = sdf.parse(data);
        gregCalendar.setTime(date);
        XMLGregorianCalendar xmlDate;
        xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregCalendar);
        
        return xmlDate;
    }
    
    private static void setPessoa(){
        AtendePessoas service = new AtendePessoas();
        AtendePessoasEJB port = service.getAtendePessoasEJBPort();
        
        Pessoa p = new Pessoa();
        p.setNome("maria");
        p.setOcupacao("empresaria");
        p.setSexo("F");
        try {
            p.setNascimento(dateToXMLCalendar("29-11-1996"));
        } catch (ParseException ex) {
            Logger.getLogger(mainApp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(mainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            port.cadastrarPessoa(p);
            System.out.println("sucesso?");
        }catch(Exception e){
            Logger.getLogger(mainApp.class.getName()).log(Level.SEVERE, null, e);
            System.out.println("erro?");
        }
    }
    
    private static void listarPessoas(){
        AtendePessoas service = new AtendePessoas();
        AtendePessoasEJB port = service.getAtendePessoasEJBPort();
        
        List<Pessoa> lista = port.listarTodasPessoas();
        
        for(Pessoa p : lista){
            System.out.println("pessoa -> " + p.getNome());
        }
        
    }
}
