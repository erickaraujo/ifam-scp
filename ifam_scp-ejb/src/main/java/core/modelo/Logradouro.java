package core.modelo;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "logradouro")
public class Logradouro extends EntidadeBase implements Serializable{
    
    @Column(nullable = false)
    private String logradouro;
    
    @Column(nullable = false)
    private String bairro;
    @Column(nullable = false)
    private String cep;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    private Cidade localidade;
    
    public Logradouro(){
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Cidade getLocalidade() {
        return localidade;
    }

    public void setLocalidade(Cidade localidade) {
        this.localidade = localidade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.logradouro != null ? this.logradouro.hashCode() : 0);
        hash = 97 * hash + (this.bairro != null ? this.bairro.hashCode() : 0);
        hash = 97 * hash + (this.cep != null ? this.cep.hashCode() : 0);
        hash = 97 * hash + (this.localidade != null ? this.localidade.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Logradouro other = (Logradouro) obj;
        if ((this.logradouro == null) ? (other.logradouro != null) : !this.logradouro.equals(other.logradouro)) {
            return false;
        }
        if ((this.bairro == null) ? (other.bairro != null) : !this.bairro.equals(other.bairro)) {
            return false;
        }
        if ((this.cep == null) ? (other.cep != null) : !this.cep.equals(other.cep)) {
            return false;
        }
        if (this.localidade != other.localidade && (this.localidade == null || !this.localidade.equals(other.localidade))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Logradouro{" + "logradouro=" + logradouro + ", bairro=" + bairro + ", cep=" + cep + ", localidade=" + localidade + '}';
    }
    
    
}
