package core.modelo;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author erick.araujo
 */


@Entity
@Table(name = "estado")
public class Estado extends EntidadeBase implements Serializable{

    @Column(nullable = false)
    private String nome;
    
    @Column(nullable = false)
    private String sigla;
    
    public Estado() {
    }

    
    
    public Estado(String nome) {
        this.nome = nome;
    }

    public Estado(String nome, String sigla) {
        this.nome = nome;
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.nome != null ? this.nome.hashCode() : 0);
        hash = 53 * hash + (this.sigla != null ? this.sigla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estado other = (Estado) obj;
        if ((this.nome == null) ? (other.nome != null) : !this.nome.equals(other.nome)) {
            return false;
        }
        if ((this.sigla == null) ? (other.sigla != null) : !this.sigla.equals(other.sigla)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estado{id = " + id + ", nome=" + nome + ", sigla=" + sigla + '}';
    }
}
