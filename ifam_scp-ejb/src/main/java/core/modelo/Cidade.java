package core.modelo;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author erick.araujo
 */

@Entity
@Table(name = "cidade")
public class Cidade extends EntidadeBase implements Serializable{
    
    @Column(nullable = false)
    private String nome;
    
    @Column(nullable = true)
    private Integer ibge;
    
    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    private Estado estado;

    public Cidade() {
    }

    public Cidade(String nome, Integer ibge, Estado estado) {
        this.nome = nome;
        this.ibge = ibge;
        this.estado = estado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    

    public Integer getIbge() {
        return ibge;
    }

    public void setIbge(Integer ibge) {
        this.ibge = ibge;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.nome != null ? this.nome.hashCode() : 0);
        hash = 17 * hash + (this.ibge != null ? this.ibge.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        
        final Cidade other = (Cidade) obj;
        
        if ((this.nome == null) ? (other.nome != null) : !this.nome.equals(other.nome)) {
            return false;
        }
        if (this.ibge != other.ibge && (this.ibge == null || !this.ibge.equals(other.ibge))) {
            return false;
        }
        return true;
    }
   
    @Override
    public String toString() {
        return "Cidade{" + "nome=" + nome + ", codigoIbge=" + ibge + '}';
    }    
}
