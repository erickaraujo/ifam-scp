/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core.modelo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.jws.WebMethod;
import javax.persistence.*;

/**
 *
 * @author erick.araujo
 */
@Entity
@Table(name = "pessoa")
public class Pessoa extends EntidadeBase implements Serializable {

    @Column(nullable = false)
    private String nome;
    @Column(nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date nascimento;
    @Column(nullable = false)
    @Transient
    private Integer idade;
    @Column(nullable = false, length = 1)
    private String sexo;
    @Column(nullable = false)
    private String ocupacao;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Logradouro logradouro;

    public Pessoa() {
    }

    public Pessoa(String nome, Date nascimento, String sexo, Logradouro logradouro) {
        this.nome = nome;
        this.nascimento = nascimento;
        this.sexo = sexo;
        this.logradouro = logradouro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    private void setIdade() {
        Calendar dataInicio = Calendar.getInstance();
        dataInicio.setTime(nascimento);

        Calendar dataHoje = Calendar.getInstance();
        Integer idade_calc = dataHoje.get(Calendar.YEAR) - dataInicio.get(Calendar.YEAR);
        this.idade = idade_calc;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
        setIdade();
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getOcupacao() {
        return ocupacao;
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
    }

    public Logradouro getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(Logradouro logradouro) {
        this.logradouro = logradouro;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + ", idade=" + idade + ", nascimento=" + nascimento + ", sexo=" + sexo + ", ocupacao=" + ocupacao + ", logradouro=" + logradouro + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.nome != null ? this.nome.hashCode() : 0);
        hash = 41 * hash + (this.nascimento != null ? this.nascimento.hashCode() : 0);
        hash = 41 * hash + (this.idade != null ? this.idade.hashCode() : 0);
        hash = 41 * hash + (this.sexo != null ? this.sexo.hashCode() : 0);
        hash = 41 * hash + (this.ocupacao != null ? this.ocupacao.hashCode() : 0);
        hash = 41 * hash + (this.logradouro != null ? this.logradouro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa other = (Pessoa) obj;
        if ((this.nome == null) ? (other.nome != null) : !this.nome.equals(other.nome)) {
            return false;
        }
        if (this.nascimento != other.nascimento && (this.nascimento == null || !this.nascimento.equals(other.nascimento))) {
            return false;
        }
        if (this.idade != other.idade && (this.idade == null || !this.idade.equals(other.idade))) {
            return false;
        }
        if ((this.sexo == null) ? (other.sexo != null) : !this.sexo.equals(other.sexo)) {
            return false;
        }
        if ((this.ocupacao == null) ? (other.ocupacao != null) : !this.ocupacao.equals(other.ocupacao)) {
            return false;
        }
        if (this.logradouro != other.logradouro && (this.logradouro == null || !this.logradouro.equals(other.logradouro))) {
            return false;
        }
        return true;
    }


}
