package core.soap.endpoints;

import core.soap.services.RandService;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.xml.ws.Endpoint;

/**
 *
 * @author erick.araujo
 */

@Startup
@Singleton
public class RandEndpoint {
    
    @PostConstruct
    public void init(){
//        ServiceEndpoint();
        System.out.println("Endpoint random test");
    }
    
    private void ServiceEndpoint() {
        final String url = "http://localhost:8888/soap";
        System.out.println("Publishing RandService at endpoint .... " +url);
        
        Endpoint.publish(url, new RandService());
    }
}
