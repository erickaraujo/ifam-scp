package core.soap.endpoints;

import core.soap.services.AtendePessoasEJB;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.xml.ws.Endpoint;

/**
 *
 * @author erick.araujo
 */

@Singleton
@Startup
public class PessoaEndpoint {
    
    @PostConstruct
    public void init(){
        System.out.println("Cadastrando endpoint pessoa");
//        pessoaEndpointInitializer();
        
    }
    
    private void pessoaEndpointInitializer(){
//        final String url = "http://localhost:8888/soap/pessoa";
//        System.out.println("Publishing PessoaService at endpoint: " + url);
//        
//        Endpoint.publish(url, new AtendePessoasEJB());
    }
}
