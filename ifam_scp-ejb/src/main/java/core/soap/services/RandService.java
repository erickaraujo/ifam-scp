package core.soap.services;

import java.util.Random;
import javax.ejb.Startup;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author erick.araujo
 */

@WebService
public class RandService {
    private static final int maxRands = 16;

    @WebMethod
    public int next1(){
        return new Random().nextInt();
    }
    
    @WebMethod
    public int[] nextN(@WebParam(name = "valor") final int n){
        final int k = (n > maxRands) ? maxRands : Math.abs(n);
        int[] rands = new int[k];
        Random r = new Random();
        for(int i=0; i<k;i++){
            rands[i] = r.nextInt();
        }
        return rands;
    }    
}
