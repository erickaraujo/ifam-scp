package core.soap.services;

import core.dao.CidadeDAO;
import core.dao.EstadoDAO;
import core.dao.LogradouroDAO;
import core.dao.PessoaDAO;
import core.modelo.Cidade;
import core.modelo.Estado;
import core.modelo.Logradouro;
import core.modelo.Pessoa;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.slf4j.Logger;

/**
 *
 * @author erick.araujo
 */
@WebService(serviceName = "AtendePessoas")
@Stateless
public class AtendePessoasEJB {
    

    @Inject
    PessoaDAO pessoa_dao;
    @Inject
    LogradouroDAO logradouro_dao;
    @Inject
    CidadeDAO cidade_dao;
    @Inject
    EstadoDAO estado_dao;

    @WebMethod(operationName = "cadastrarPessoa")
    public String cadastrarPessoa(@WebParam(name = "pessoa") Pessoa p) {
        if (p == null) {
            return "Erro ao Cadastrar! Pessoa vazia";
        }
        pessoa_dao.inserir(p);
        return p.getNome() + " cadastrado com Sucesso!";
    }

    @WebMethod(operationName = "editarPessoa")
    public String atualizarPessoa(@WebParam(name = "pessoa") Pessoa p) {
        if (p == null) {
            return "Erro ao Atualizar! Pessoa vazia";
        }
        pessoa_dao.atualizar(p);
        return p.getNome() + " editada com Sucesso!";
    }

    @WebMethod(operationName = "consultarPessoa")
    public Pessoa consultarPessoa(@WebParam(name = "pessoa") Pessoa p) {
        if (p == null) {
            return null;
        }
        return pessoa_dao.consultar(p.getId());
    }

    @WebMethod(operationName = "pesquisaPessoaPorNome")
    public List<Pessoa> pesquisaPessoaPorNome(@WebParam(name = "nome") String nome) {
        if (nome == null) {
            return null;
        }
        return pessoa_dao.pesquisaPorNome(nome);
    }

    @WebMethod(operationName = "listarTodasPessoas")
    public List<Pessoa> listarTodasPessoas() {
        return pessoa_dao.listarTodos();
    }

    @WebMethod(operationName = "deletaPessoa")
    public String deletaPessoa(@WebParam(name = "pessoa") Pessoa p) {
        if (p == null) {
            return "Erro ao Deletar! Pessoa vazia";
        }
        pessoa_dao.remover(p);
        return p.getNome() + " removido com Sucesso!";
    }

    public String cadastraCep(@WebParam(name = "logradouro") Logradouro cepNovo) {
        if (cepNovo == null) {
            return "Erro ao cadastrar Logradouro";
        }
        Logradouro cepExistente = logradouro_dao.consultarPorCep(cepNovo.getCep());
        if (cepExistente == null) {
            logradouro_dao.inserir(cepNovo);
            return "CEP " + cepNovo.getCep() + " não existente, portanto cadastrado com sucesso";
        } else {
            cepExistente.setLocalidade(cepNovo.getLocalidade());
            cepExistente.setLogradouro(cepNovo.getLogradouro());
            cepExistente.setCep(cepNovo.getCep());
            cepExistente.setBairro(cepNovo.getBairro());
            logradouro_dao.atualizar(cepExistente);
            return "CEP " + cepNovo.getCep() + " atualizado com sucesso";
        }
    }

    @WebMethod
    public Logradouro consultaCep(@WebParam(name = "cep") String cep) {
        if (cep.isEmpty()) {
            return null;
        }

        return logradouro_dao.consultarPorCep(cep);
    }

    @WebMethod
    public Logradouro consultaCepPorId(@WebParam(name = "id") Integer id) {
        if (id == null) {
            return null;
        }
        return logradouro_dao.consultar(id);
    }

    @WebMethod
    public List<Logradouro> listarTodosCeps() {
        return logradouro_dao.listarTodos();
    }

    @WebMethod
    public void cadastrarCidade(@WebParam(name = "cidade") Cidade cidadeNova) {
        if (cidadeNova == null) {
            System.out.println("Cidade nula ou invalida");
        }
        Cidade cidadeExistente = cidade_dao.listarCidadePorNomeEstado(cidadeNova.getNome(), cidadeNova.getEstado().getNome());
        if (cidadeExistente == null) {
            cidade_dao.inserir(cidadeNova);
            System.out.println("cidade '" + cidadeNova.getNome() + "' cadastrada com sucesso");
        } else {
            cidadeExistente.setNome(cidadeNova.getNome());
            cidadeExistente.setEstado(cidadeNova.getEstado());
            cidadeExistente.setIbge(cidadeNova.getIbge());
            cidadeExistente.setEstado(cidadeNova.getEstado());

            cidade_dao.atualizar(cidadeExistente);
            System.out.println("cidade '" + cidadeNova.getNome() + "' atualizada com sucesso");
        }
    }

    @WebMethod
    public List<Cidade> listarTodasCidades() {
        return cidade_dao.listarTodos();
    }

    @WebMethod
    public Cidade consultarCidadePorId(@WebParam(name = "cidade") Cidade cid) {
        if (cid == null) {
            return null;
        }

        Cidade cidade = cidade_dao.consultar(cid.getId());
        return cidade;
    }

    @WebMethod
    public List<Cidade> listaCidadePorNome(@WebParam(name = "cidade") String cidade_nome) {
        if (cidade_nome.isEmpty()) {
            return null;
        }
        return cidade_dao.listarCidadePorNome(cidade_nome);
    }
    
    @WebMethod
    public Cidade listaCidadePorNomedEstado(
            @WebParam(name="cidade_nome") String cidade_nome,
            @WebParam(name="estado_nome") String estado_nome){
        if(cidade_nome.isEmpty() || estado_nome.isEmpty()){
            return null;
        }
        
        return cidade_dao.listarCidadePorNomeEstado(cidade_nome, estado_nome);
    }

    @WebMethod
    public List<Cidade> listaCidadePorEstado(@WebParam(name = "estado_nome") String estado_nome) {
        if (estado_nome.isEmpty()) {
            return null;
        }
        return cidade_dao.listarCidadePorEstado(estado_nome);
    }

    @WebMethod
    public String cadastrarEstado(@WebParam(name = "estado") Estado estadoNovo) {
        if (estadoNovo == null) {
            return "estado inválido ou nulo";
        }
        Estado estadoExistente = estado_dao.consultarPorNome(estadoNovo.getNome());
        if (estadoNovo.getNome() == null) {
            estadoExistente = estado_dao.consultarPorSigla(estadoNovo.getSigla());
        } else if (estadoNovo.getSigla() == null) {
            estadoExistente = estado_dao.consultarPorNome(estadoNovo.getNome());
        }

        if (estadoExistente == null) {
            estado_dao.inserir(estadoNovo);
            return "Estado '" + estadoNovo.getNome() + "' cadastrado com sucesso";
        } else {
            estadoExistente.setNome(estadoNovo.getNome());
            estado_dao.atualizar(estadoExistente);
            return "Estado '" + estadoNovo.getNome() + "' atualizado com sucesso";
        }
    }

    @WebMethod
    public String atualizarEstado(@WebParam(name = "estado") Estado estado) {
        if (estado == null) {
            return "estado inválida ou nula!";
        }

        estado_dao.atualizar(estado);
        return "Estado '" + estado.getNome() + "' atualizado com sucesso";
    }

    @WebMethod
    public List<Estado> listarTodosEstados() {
        return estado_dao.listarTodos();
    }

    @WebMethod
    public Estado listarEstadosPorNome(String estado) {
        return estado_dao.consultarPorNome(estado);
    }
    
    @WebMethod
    public Estado listarEstadoPorSigla(String sigla){
        return estado_dao.consultarPorSigla(sigla);
    }

    @WebMethod
    public Estado consultarEstadoPorId(@WebParam(name = "estado") Estado cid) {
        if (cid == null) {
            return null;
        }

        Estado estado = estado_dao.consultar(cid.getId());
        return estado;
    }

    @WebMethod(operationName = "toString")
    public String toString(Pessoa p) {
        return p.toString();
    }
}
