package core.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;


public class BaseDAO<E> {

    @Inject
    private org.slf4j.Logger log;
    
    private EntityManager em;
    private Class<E> classe;
    
    public BaseDAO(EntityManager em, Class<E> classe){
        this.em = em;
        this.classe = classe;
    }
    
    public void inserir(E entidade){
         em.persist(entidade);
/*
**        Parallel idea to prevent any duplicated value based on a single column
*/        
        
//        try {
//            Method getNome = entidade.getClass().getDeclaredMethod("getNome", new Class[0]);
//            String nome = (String) getNome.invoke(entidade);
//
//            TypedQuery<E> query = (TypedQuery<E>) em.createQuery("select t from "+ entidade.getClass().getName()+ " t where t.nome = :nome", entidade.getClass())
//                    .setParameter("nome", nome);
//            E entity = query.getSingleResult();
//            if(entity == null){
//                em.persist(entidade);
//            }
//            else {
//                Method getId = entity.getClass().getMethod("getId", new Class[0]);
//                Integer id = (Integer) getId.invoke(entity);
//                entity = entidade;
//                Method setId = entity.getClass().getMethod("setId", Integer.class);
//                setId.invoke(entity, id);
//                em.merge(entity);
//            }
//        } catch (NoSuchMethodException ex) {
//            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SecurityException ex) {
//            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
       
    }
    
    public E consultar(Integer id){
        return em.find(classe, id);
    }
    
    public List<E> listarTodos(){
        return em.createQuery("select t from "+ classe.getName() +" t", classe)
                .getResultList();
    }
    
    public void remover(E entidade){
        em.remove(em.contains(entidade) ? entidade : em.merge(entidade));
    }
    
    public E atualizar(E entidade){
        return em.merge(entidade);
    }
}
