package core.dao;

import core.modelo.Pessoa;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author erick.araujo
 */
public class PessoaDAO {
    @PersistenceContext
    private EntityManager em;
    private BaseDAO<Pessoa> dao;
    
    @PostConstruct
    public void init(){
        dao = new BaseDAO(em, Pessoa.class);
    }

    public void inserir(Pessoa entidade) {
        try {
            dao.inserir(entidade);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public Pessoa consultar(Integer id) {
        return dao.consultar(id);
    }

    public List<Pessoa> listarTodos() {
        return dao.listarTodos();
    }

    public List<Pessoa> pesquisaPorNome(String nome){
        TypedQuery<Pessoa> query = em.createQuery("select p from Pessoa p where p.nome like :nome order by p.nome", Pessoa.class);
        query.setParameter("nome", "%" + nome + "%");
        return query.getResultList();
    }
            
    public void remover(Pessoa entidade) {
        dao.remover(entidade);
    }

    public Pessoa atualizar(Pessoa entidade) {
        return dao.atualizar(entidade);
    }
}
