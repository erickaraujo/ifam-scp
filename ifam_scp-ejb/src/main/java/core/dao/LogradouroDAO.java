package core.dao;

import core.modelo.Logradouro;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author erick.araujo
 */
public class LogradouroDAO {
    
    @PersistenceContext
    private EntityManager em;
    private BaseDAO<Logradouro> dao;
    
    @PostConstruct
    public void init(){
        dao = new BaseDAO(em, Logradouro.class);
    }

    public void inserir(Logradouro entidade) {
        try {
            dao.inserir(entidade);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public Logradouro consultar(Integer id) {
        return dao.consultar(id);
    }

    public List<Logradouro> listarTodos() {
        return dao.listarTodos();
    }
    
    public Logradouro consultarPorCep(String cep){
        Query query = em.createQuery("select t, c, e from Logradouro t, Cidade c, Estado e where t.cep like :cep").
                setParameter("cep", "%"+cep+"%");
        try{
            Logradouro logr = (Logradouro) query.getSingleResult();
            System.out.println("logradouro -> " + logr.toString());
            return logr;
        }catch(NoResultException nre){
            return null;
        }
    }

    public void remover(Logradouro entidade) {
        dao.remover(entidade);
    }

    public Logradouro atualizar(Logradouro entidade) {
        return dao.atualizar(entidade);
    }
    
    
}
