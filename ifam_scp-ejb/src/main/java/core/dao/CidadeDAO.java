package core.dao;

import core.modelo.Cidade;
import core.modelo.Estado;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author erick.araujo
 */
public class CidadeDAO {

    @PersistenceContext
    private EntityManager em;
    private BaseDAO<Cidade> dao;
    
    @PostConstruct
    public void init(){
        dao = new BaseDAO(em, Cidade.class);
    }

    public void inserir(Cidade entidade) {
            dao.inserir(entidade);
    }

    public Cidade consultar(Integer id) {
        return dao.consultar(id);
    }
    
    public Cidade listarCidadePorNomeEstado(String cidade, String estado){
        TypedQuery<Cidade> query = em.createQuery("select c from Cidade c inner join c.estado e where c.nome like :cidade and e.nome like :estado", Cidade.class)
                .setParameter("cidade", "%"+cidade+"%")
                .setParameter("estado", "%"+estado+"%");
//        List<Object[]> resultList = query.getResultList();
//        List<Cidade> resultCidades = new ArrayList(resultList.size());
//        for(Object[] res : resultList){
//            Estado e = new Estado(res[2].toString(), res[3].toString());
//            Cidade c = new Cidade(res[0].toString(), Integer.valueOf(res[1].toString()), e);
//            resultCidades.add(c);
//        }
        return query.getSingleResult();
    }
    
    public List<Cidade> listarCidadePorEstado(String estado){
        TypedQuery<Cidade> query = em.createQuery("select c from Cidade c inner join c.estado e where e.nome like :estado", Cidade.class)
                .setParameter("estado", "%"+estado+"%");
        return query.getResultList();
    }
    
    public List<Cidade> listarCidadePorNome(String cidade){
        TypedQuery<Cidade> query = em.createQuery("select c from Cidade c where c.nome like :cidade ", Cidade.class)
                .setParameter("cidade", "%"+cidade+"%");
        
        return query.getResultList();
    }

    public List<Cidade> listarTodos() {
        return dao.listarTodos();
    }

    public void remover(Cidade entidade) {
        dao.remover(entidade);
    }

    public Cidade atualizar(Cidade entidade) {
        return dao.atualizar(entidade);
    }
    
    
}
