package core.dao;

import core.modelo.Estado;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author erick.araujo
 */
public class EstadoDAO{
    
    @PersistenceContext
    private EntityManager em;
    private BaseDAO<Estado> dao;

    @PostConstruct
    private void init(){
        dao = new BaseDAO(em, Estado.class);
    }

    public void inserir(Estado entidade) {
        try {
            System.out.println("estadoDAO ->" + entidade.toString());
            dao.inserir(entidade);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public Estado consultar(Integer id) {
        return dao.consultar(id);
    }
    
    public Estado consultarPorNome(String nome){
        TypedQuery<Estado> query = em.createQuery("select e from Estado e where e.nome like :nome", Estado.class)
                .setParameter("nome", "%"+nome+"%");
        
        return query.getSingleResult();
    }
    
    public Estado consultarPorSigla(String nome){
        TypedQuery<Estado> query = em.createQuery("select e from Estado e where e.sigla like :nome", Estado.class)
                .setParameter("nome", "%"+nome+"%");
        
        return query.getSingleResult();
    }
    
    public Estado atualizar(Estado entidade) {
        return dao.atualizar(entidade);
    }

    public void remover(Estado entidade) {
        dao.remover(entidade);
    }

    public List<Estado> listarTodos() {
        return dao.listarTodos();
    }
}
