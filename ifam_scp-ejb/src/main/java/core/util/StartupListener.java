package core.util;

import core.dao.CidadeDAO;
import core.dao.EstadoDAO;
import core.dao.LogradouroDAO;
import core.dao.PessoaDAO;
import core.modelo.Cidade;
import core.modelo.Estado;
import core.modelo.Logradouro;
import core.modelo.Pessoa;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;

/**
 *
 * @author erick.araujo
 */

@Startup
@Singleton
public class StartupListener implements Serializable{
    
    @PersistenceContext
    private EntityManager em;
    @Inject
    private EstadoDAO estadoDAO;
    @Inject
    private CidadeDAO cidadeDAO;
    @Inject
    private LogradouroDAO logradouroDAO;
    @Inject
    private PessoaDAO pessoaDAO;
    
    @Inject
    private Logger log;
    
    @PostConstruct
    public void init(){
        try {
            populaEstados();
            //        testaCidade();
//        testaLogradouro();
            populaPessoas();
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(StartupListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void populaEstados(){
        log.info("popula estados");
        TypedQuery<Estado> query = em.createQuery("select e from Estado e", Estado.class);
        query.setMaxResults(1);
        List<Estado> cidades = query.getResultList();
        
        if(cidades.isEmpty()){
            String line;
            String csvSplit = ",";
            
            System.out.println("**** STARTUP LISTENER ****");
            try{
                InputStream is = getClass().getResourceAsStream("/lista_estados.csv");
                InputStreamReader file = new InputStreamReader(is);
                BufferedReader buffer = new BufferedReader(file);
                
                buffer.readLine();
                while((line = buffer.readLine()) != null){
                    String[] estados = line.split(csvSplit);
                    
                    System.out.println("Estados: " + Arrays.toString(estados));
                    System.out.println("estado: " + estados[2]+ ", " + estados[1] + ", " + estados[3]);
                    Estado estado = new Estado(estados[2].replace("\"", ""), estados[1].replace("\"", ""));
                    Cidade cidade = new Cidade(estados[3].replace("\"", ""), 0, estado);
                    estadoDAO.inserir(estado);
                    cidadeDAO.inserir(cidade);
                }
            }
            catch(IOException e){
                log.info(": ", e.getMessage());
            }
        }
    }
    
    private void testaCidade(){
        log.info("testing cidade");
        try{
            Estado estado = new Estado("Amazonas", "AM");
            estadoDAO.inserir(estado);
        }catch(Exception e){
            log.info(": ", e.getMessage());
        }
    }
    
    private void testaLogradouro(){
        Logradouro logr = new Logradouro();
        logr.setCep("69070730");
        logr.setBairro("Colonia Oliveira Machado");
        logr.setLogradouro("rua joao vicente");
        Cidade cid = cidadeDAO.listarCidadePorNomeEstado("Manaus", "Amazonas");
        System.out.println("cidade -> " + cid.toString());
//        List<Cidade> cidade = cidadeDAO.listarCidadePorNomeEstado("Manaus", "Amazonas");
//        for(Cidade c : cidade){
//            System.out.println("c -> " + c.toString());
//            System.out.println("e -> " + c.getEstado().toString());
        
        
        
//        logr.setLocalidade(cid);
//        logradouroDAO.inserir(logr);
        
        
        List<Logradouro> ceps = logradouroDAO.listarTodos();
        
        System.out.println("ceps existentes -> " + ceps);
        
        logr = logradouroDAO.consultarPorCep("69005130");
        if(logr == null){
            System.out.println("logradouro não encontrado!");
        }else{
            System.out.println("cep 69005130 -> " + logr);
        }
    }
    
    private void populaPessoas() throws ParseException{
        Pessoa p1 = new Pessoa();
        p1.setNome("joao");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date d = sdf.parse("10-02-1996");
        p1.setNascimento(d);
        
        p1.setOcupacao("Musico");
        p1.setSexo("M");
        Logradouro l1 = new Logradouro();
        l1.setBairro("Centro");
        l1.setCep("000000000");
        l1.setLogradouro("rua 1");
        Cidade cidade = cidadeDAO.listarCidadePorNomeEstado("Manaus", "Amazonas");
        l1.setLocalidade(cidade);
        p1.setLogradouro(l1);
        
        logradouroDAO.inserir(l1);
        pessoaDAO.inserir(p1);
    }
}
